# Project_matplotlib

This is the project was presented to the course Applied Plotting, Charting & Data Representation in Python.

Links Dataset:

- https://www.datos.gov.co/Ambiente-y-Desarrollo-Sostenible/Precipitaciones-Totales-Mensuales/mb4n-6m2g
- https://www.datos.gov.co/Ambiente-y-Desarrollo-Sostenible/Emergencias-Naturales/xjv9-mim9
- https://www.datos.gov.co/Ambiente-y-Desarrollo-Sostenible/Cat-logo-Nacional-de-Estaciones-del-IDEAM/hp9r-jxuu
- https://datosgeograficos.car.gov.co/datasets/04445258819e4016b2296332e0f50f78_0/data?geometry=-74.708%2C4.328%2C-72.753%2C4.807
- https://www.datos.gov.co/Ambiente-y-Desarrollo-Sostenible/N-mero-de-d-as-con-precipitaci-n/jhyk-8pw6


